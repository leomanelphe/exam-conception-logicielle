import requests
import os
from dotenv import load_dotenv

load_dotenv()
SERVER_URL = os.getenv("SERVER_URL")


def init_deck():
    request = requests.get(SERVER_URL + '/creer-un-deck/')
    return request.json()["deck_id"]


def draw_cards_from_deck(nombre_cartes, deck_id):
    http_request = SERVER_URL + '/cartes/' + \
        str(nombre_cartes) + '/?deck_id=' + deck_id
    request = requests.post(http_request)
    return request.json()["cards"]


def nb_per_suit(cards):
    counter = {"H": 0, "S": 0, "D": 0, "C": 0}
    for card in cards:
        suit = card.get("code")[1]
        counter[suit] += 1
    return counter
