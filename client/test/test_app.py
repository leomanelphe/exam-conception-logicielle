import unittest
from client.main import nb_per_suit


class TestMethod(unittest.TestCase):
    def test_nb_per_suit(self):
        cards = [{
            "image": "https://deckofcardsapi.com/static/img/8C.png",
            "value": "8",
            "suit": "CLUBS",
            "code": "8C"
        }, {
            "image": "https://deckofcardsapi.com/static/img/AS.png",
            "value": "A",
            "suit": "SPADES",
            "code": "AS"
        }]
        expectedCounter = {"H": 0, "S": 1, "D": 0, "C": 1}
        self.assertEqual(nb_per_suit(cards), expectedCounter)


if __name__ == "__main__":
    unittest.main()
