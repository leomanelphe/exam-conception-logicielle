import requests
import uvicorn
from fastapi import FastAPI
import os
from dotenv import load_dotenv

load_dotenv()
SERVER_URL = os.getenv("SERVER_URL")
app = FastAPI()


@app.get("/")
def root():
    return {"result": "ok"}


@app.get('/creer-un-deck/')
def read_deck():
    request = requests.get(
        SERVER_URL + '/deck/new/shuffle/?deck_count=1')
    deck_id = request.json()["deck_id"]
    return {"deck_id": deck_id}


@app.post('/cartes/{nombre_cartes}/')
async def draw_cards(nombre_cartes: int, deck_id: str):
    http_request = SERVER_URL + '/deck/' + \
        deck_id + '/draw/?count=' + str(nombre_cartes)
    request = requests.post(http_request)
    cards = request.json()
    return cards


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
