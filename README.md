# Examen de conception logicielle

Par manque de temps, ce rendu est incomplet. Il manque notamment de la documentation et je n'ai pas pu tout tester.

Pour une raison que je n'ai pas pu identifier, l'exécutable n'arrive qu'à compter une carte au lieu de 10, et il lui arrive parfois de rencontrer une erreur 500.

## Exécutable

Le fichier `executable.py` contient un scénario d'utilisation du webservice par la partie client.

Lancer d'abord le webservice avec `python webservice/main.py`

Lancer ensuite le scénario avec `python executable.py`

## Test

Un test a été réalisé pour la fonction nb_per_suit de la partie client.

Pour l'exécuter, entrer la commande `python -m unittest client/test/test_app.py`